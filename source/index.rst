A C++ Book
==========

.. image:: _static/ISO_C++_Logo.png
    :alt: C++ Logo

for Undergraduate Computer Science Majors
-----------------------------------------

by Jeffrey Elkner (with liberal borrowings from the work of Allen B. Downey and
Paul Bui)

illustrated by Natalia Cerna 

Last Updated: 6 March 2025

* `Copyright Notice <copyright.html>`__ 
* `Forward <forward.html>`__
* `Preface <preface.html>`__
* `Contributor List <contrib.html>`__
* `Chapter 1 <ch01.html>`__ *The way of the program*
* `Chapter 2 <ch02.html>`__ *Variables, types, and expressions*
* `Chapter 3 <ch03.html>`__ *Functions*
* `Chapter 4 <ch04.html>`__ *Conditionals and recursion*
* `Chapter 5 <ch05.html>`__ *Fruitful functions*
* `Chapter 6 <ch06.html>`__ *Iteration*
* `Chapter 7 <ch07.html>`__ *Strings*
* `Chapter 8 <ch08.html>`__ *Structures*
* `Chapter 9 <ch09.html>`__ *Pointers and arrays*
* `Chapter 10 <ch10.html>`__ *Vectors*
* `Chapter 11 <ch11.html>`__ *Member functions*
* `Chapter 12 <ch12.html>`__ *Vectors of objects*
* `Chapter 13 <ch13.html>`__ *Objects of vectors*
* `Chapter 14 <ch14.html>`__ *Classes and invarients*
* `Chapter 15 <ch15.html>`__ *Extending classes*
* `Chapter 16 <ch16.html>`__ *Vectors of vectors*
* `Chapter 17 <ch17.html>`__ *Linked lists*
* `Chapter 18 <ch18.html>`__ *Stacks*
* `Chapter 19 <ch19.html>`__ *Queues and priority queues*
* `Chapter 20 <ch20.html>`__ *Trees*
* `Chapter 21 <ch21.html>`__ *Heaps*
* `Appendix A <app_a.html>`__ *Code source*
* `Appendix B <app_b.html>`__ *Debugging*
* `Appendix C <app_c.html>`__ *A development environment for unit testing*
* `Appendix D <app_d.html>`__ *Customizing and contributing to the book*
* `GNU Free Document License <fdl-1.3.html>`__ 

.. toctree::
    :maxdepth: 1
    :hidden:

    copyright.rst
    forward.rst
    preface.rst
    contrib.rst

.. toctree::
    :maxdepth: 1
    :numbered:
    :hidden:

    ch01.rst
    ch02.rst
    ch03.rst
    ch04.rst
    ch05.rst
    ch06.rst
    ch07.rst
    ch08.rst
    ch09.rst
    ch10.rst
    ch11.rst
    ch12.rst
    ch13.rst
    ch14.rst
    ch15.rst
    ch16.rst
    ch17.rst
    ch18.rst
    ch19.rst
    ch20.rst
    ch21.rst

.. toctree::
    :maxdepth: 1
    :hidden:

    app_a.rst
    app_b.rst
    app_c.rst
    app_d.rst
    fdl-1.3.rst

.. toctree::
    :maxdepth: 1
    :hidden:
    :glob:

    exercises/ch??/* 
    exercises/solutions/ch??/* 

* :ref:`search`
