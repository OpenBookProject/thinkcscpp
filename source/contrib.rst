Contributor List
================

by Jeffrey Elkner

To paraphrase the philosophy of the Free Software Foundation, this book is free
like free speech, but not necessarily free like free pizza. It came about
because of a collaboration that would not have been possible without the GNU
Free Documentation License. So we would like to thank the Free Software
Foundation for developing this license and, of course, making it available to
us.

We would also like to thank the sharp-eyed and thoughtful readers who have sent
us suggestions and corrections over the years. In the spirit of free software,
we decided to express our gratitude in the form of a contributor list.
Unfortunately, this list is far from complete.  It gets too large to include
everyone who sends in a typo or two. You have our gratitude, and you have the
personal satisfaction of making a book you found useful better for you and
everyone else who uses it.

If you have a chance to look through the list, you should realize that each
person here has spared you and all subsequent readers from the confusion of a
technical error or a less-than-transparent explanation, just by sending us a
note.

If you should stumble across an error, we hope you will take a minute to
contact us. The email address is `jeff@elkner.net <mailto:jeff@elkner.net>`__ .
Substantial changes made due to your suggestions will add you to the next
version of the contributor list (unless you ask to be omitted). Thank you!


Third Edition of 2024
---------------------

I am using this book this year for use in two computer science classes offered
through `Northern Virginia Community College
<https://en.wikipedia.org/wiki/Northern_Virginia_Community_College>`_ during
the 2024-25 academic year, `CSC 222: Object-Oriented Programming
<https://courses.vccs.edu/courses/CSC222>`_ during the Fall 2024 semester,
and `CSC 223: Data Structures and Analysis of Algorithms
<https://courses.vccs.edu/courses/CSC223>`_ during the Spring 2025 semester.

`Brayden Zee <https://github.com/BlueZeeKing>`_ has been taking charge this
year of adapting the missing chapters from Allen Downey's `Think Java 2e
<https://greenteapress.com/wp/think-java-2e/>`_ into our new C++ book. He
has also been writing `ncurses <https://en.wikipedia.org/wiki/Ncurses>`_
example programs and showing us how to use
`make <https://en.wikipedia.org/wiki/Make_(software)>`_, both of which will
be incorporated into the exercises in the later chapters of the book.


Second Edition of 2023
----------------------

I revivied this book for use in two computer science classes offered through
`Northern Virginia Community College
<https://en.wikipedia.org/wiki/Northern_Virginia_Community_College>`_ during
the 2023-24 academic year,
`CSC 222: Object-Oriented Programming
<https://courses.vccs.edu/courses/CSC222>`_ and
`CSC 223: Data Structures and Analysis of Algorithms
<https://courses.vccs.edu/courses/CSC223>`_.

I teach these classes through our dual-enrolled computer science program at the
`Governor's Career and Technical Academy in Arlington (GCTAA)
<https://www.doe.virginia.gov/teaching-learning-assessment/k-12-standards-instruction/career-and-technical-education-cte/governor-s-stem-academies/established-stem-academies/governor-s-career-and-technical-academy-in-arlington>`_. Students
in my classes this year have played a crucial role in the development of the
new edition of this book.

`Nathaniel Levin <https://github.com/NateLevin1>`_ led the group of students
who volunteered to port Java source code from Allen Downey's `Think Java 2e
<https://greenteapress.com/wp/think-java-2e/>`_ into C++.


First Edition of 2002
---------------------

As a busy student in his senior year at `Yorktown High School
<https://en.wikipedia.org/wiki/Yorktown_High_School_(Arlington_County,_Virginia)>`_
in Arlington, VA, `Paul Bui <https://www.paulbui.net/>`_ took on as a project
the adaption of this book from its original version using Java to this version
using C++.

Paul was one of those students who took the
`AP Computer Science <https://en.wikipedia.org/wiki/AP_Computer_Science>`_
exam (he took the discontinued *AP Computer Science AB* exam) during the
four years it was given in C++.

Together with classmates Jonah Cohen, Charles Harrison, Donald Oellerich,
Drew Stephens, and his brother Peter Bui, they produced the first version of
this textbook in C++. They are were all high school students when they did
that, so this is a wonderful example of the power of `open educational
resources <https://en.wikipedia.org/wiki/Open_educational_resources>`_ to
enage learners in high level learning.

More than two decades later, Paul is now a veteran computer science teacher at
`Washington-Liberty High School
<https://en.wikipedia.org/wiki/Washington-Liberty_High_School>`_ in the same
Arlington County School system from which he graduated.
