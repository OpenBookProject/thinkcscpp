#include <iostream>
using namespace std;

int f(char x, float y) {
    return 2 * x + y;
}

int f(float x, char y) {
    return x - y;  
}

int f(int a, int b, int c) {
    int total = 0;
    while (--c)
        total += b;
    return total - a;
}

int main() {
    char c1 = 3;
    char c2 = 5;
    cout << f(1, 4, 2) << f(c2, 2.0) << f(4.0, c1) << endl;
    return 0;
}
