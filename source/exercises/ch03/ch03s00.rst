..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch03s00:

Chapter 3 Exercise Set 0: Chapter Review
========================================

#. Rearrange the program presented in :ref:`adding_new_functions` so that
   the definition of ``new_line`` is moved *after* the definition of
   ``main``. Try to compile it and note what happens.

#. Add several calls to the ``print_twice`` function in
   :ref:`parameters_and_arguments` with different values as arguments. What
   happens if you send it an ``int`` instead of a ``char``? What about a
   ``double``?  Try sending a string, like, ``"tryme"``, as an argument.
   What happens when you compile it then?

#. Write programs to answer the two unanswered questions in the
   :ref:`functions_with_results` section.
