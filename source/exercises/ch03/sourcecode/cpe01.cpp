#include <iostream>
using namespace std;

int main() {
    for (float f = -4.0; f < 500; f *= -2)
        cout << '*';
    cout << endl;

    return 0;
}
