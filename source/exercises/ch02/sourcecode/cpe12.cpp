#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    cout << n << hex << n + n << oct << n << endl;

    return 0;
}
