#include <iostream>
using namespace std;

int strlen(char s[]){
    int i = 0;
    while (s[i] != '\0') i++;
    return i;
}

int main() {
    char s[] = "Arlington Career Center\0";

    cout << s << " is " << strlen(s) << " characters long.\n";

    return 0;
}
