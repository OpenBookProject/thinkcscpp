#include <iostream>
#include <string>
using namespace std;

template <class T>
class Node
{
  public:
    T cargo;
    Node<T>* next;

    Node(T cargo, Node<T>* next) {
        this->cargo = cargo;
        this->next = next;
    }
};

template <class T>
class LinkedList
{
  public:
    int num_nodes;
    Node<T>* head;

    // constructors
    LinkedList() {
        num_nodes = 0;
        head = NULL;
    }

    LinkedList(T cargo) {
        num_nodes = 1;
        head = new Node<T>(cargo, NULL);
    }

    // member functions
    int length() const {
        return num_nodes;
    }

    string to_str() const {
        string s = "(";
        Node<T>* node = head;
        while (node != NULL) {
            s += to_string(node->cargo);
            node = node->next;
            if (node != NULL) s += ", ";
        }
        return s + ")";
    }

    // modifiers
    void insert_at_front(T cargo) {
        Node<T>* front = new Node<T>(cargo, head);
        head = front;
        num_nodes++;
    }

    T remove_from_front() {
        if (head == NULL) throw runtime_error("Can't remove from empty list!");
        T cargo = head->cargo;
        Node<T>* front = head;
        head = head->next;
        delete front;
        num_nodes--;
        return cargo;
    }
};
