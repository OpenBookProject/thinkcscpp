#include <iostream>
#include <string>
#include "linked_list.h"
using namespace std;

void print_list(Node* list) {
    Node* node = list;
    while (node != NULL) {
        cout << node->cargo;
        node = node->next;
        if (node != NULL) cout << ", ";
    }
}

int main()
{
    Node* node1 = new Node(1, NULL);
    Node* node2 = new Node(2, NULL);
    Node* node3 = new Node(3, NULL);

    node1->next = node2;
    node2->next = node3;

    print_list(node1);

    return 0;
}
