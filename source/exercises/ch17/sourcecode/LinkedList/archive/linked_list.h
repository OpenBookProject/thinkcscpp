#include <iostream>
#include <string>
using namespace std;

struct Node {
    int cargo;
    Node* next;

    // constructors
    Node();
    Node(int cargo, Node* next_node);

    // member functions
    string to_str() const;
};

void print_list(Node* list);
