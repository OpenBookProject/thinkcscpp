#include <iostream>
#include <string>
#include "linked_list.h"
using namespace std;

void print_backward(Node* list) {
    if (list == NULL) return;

    Node* head = list;
    Node* tail = list->next;

    print_backward(tail);
    cout << head->cargo;
}

int main()
{
    Node* node1 = new Node(1, NULL);
    Node* node2 = new Node(2, NULL);
    Node* node3 = new Node(3, NULL);

    node1->next = node2;
    node2->next = node3;

    print_backward(node1);
    cout << endl;

    return 0;
}
