#include <iostream>
#include <string>
#include "linked_list.h"
using namespace std;

Node* remove_second(Node* list) {
    Node* first = list;
    if (first->next == NULL || first->next->next == NULL) return NULL;
    Node* second = list->next;

    // make the first node point to the third
    first->next = second->next;

    // remove the second node from the list and return a pointer to it
    second->next = NULL;
    return second;
}

int main()
{
    Node* node1 = new Node(1, NULL);
    Node* node2 = new Node(2, NULL);
    Node* node3 = new Node(3, NULL);

    node1->next = node2;
    node2->next = node3;

    print_list(node1);
    Node* second_node = remove_second(node1);
    print_list(second_node);
    print_list(node1);
    cout << endl;

    return 0;
}
