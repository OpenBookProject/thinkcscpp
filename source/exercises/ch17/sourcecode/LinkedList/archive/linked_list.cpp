#include <iostream>
#include <string>
#include "linked_list.h"
using namespace std;

// constructors
Node::Node() {
    cargo = 0;
    next = NULL;
}

Node::Node(int the_cargo, Node* next_node) {
    cargo = the_cargo;
    next = next_node;
}

// member functions
string Node::to_str() const {
    return to_string(cargo);
}

void print_list(Node* list) {
    Node* node = list;
    while (node != NULL) {
        cout << node->cargo;
        node = node->next;
        if (node != NULL) cout << ", ";
    }
}
