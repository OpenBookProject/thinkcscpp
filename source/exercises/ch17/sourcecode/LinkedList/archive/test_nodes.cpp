#include <iostream>
#include <string>
#include "linked_list.h"
using namespace std;

int main()
{
    Node* node1 = new Node(1, NULL);
    Node* node2 = new Node(2, NULL);
    Node* node3 = new Node(3, NULL);

    node1->next = node2;
    node2->next = node3;

    Node* node = node1;

    while (node != NULL) {
        cout << node->cargo << endl;
        node = node->next;
    }

    return 0;
}
