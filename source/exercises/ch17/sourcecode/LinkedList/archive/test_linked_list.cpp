#include <iostream>
#include <string>
#include "LinkedList.h"
using namespace std;

int main()
{
    LinkedList<int>* list = new LinkedList<int>();
    cout << "list length: " << list->length() << endl;
    list->insert_at_front(42);
    list->insert_at_front(24);
    list->insert_at_front(6);
    list->insert_at_front(55);
    cout << "list: " << list->to_str() << endl;
    for (int i = 0; i < 5; i++) {
        cout << "Removing " << list->remove_from_front() << endl;
        cout << "list: " << list->to_str() << endl;
    }

    return 0;
}
