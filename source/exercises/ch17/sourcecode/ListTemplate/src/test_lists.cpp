#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <string>
#include "LinkedList.h"
using namespace std;

TEST_CASE("Test basic list of strings operations") {
    LinkedList<string> toppings;
    toppings.insert_at_front("cheese");
    CHECK(toppings.head->cargo == "cheese");
    toppings.insert_at_front("anchovies");
    CHECK(toppings.head->cargo == "anchovies");
    toppings.insert_at_front("onions");
    CHECK(toppings.num_nodes == 3);
    CHECK(toppings.remove_from_front() == "onions");
}
