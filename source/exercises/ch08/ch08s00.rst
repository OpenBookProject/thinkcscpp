..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch08s00:

Chapter 8 Exercise Set 0: Chapter Review
========================================

1. Write function named ``lower_right`` that takes a ``Rectangle`` as an
   argument and returns a ``Point`` with the location of the lower right hand
   corner.


2. Write a function named ``find_area`` that takes a ``Rectangle`` as an
   argument and returns the area of the rectangle.


3. Use the ``sizeof`` operator you first met back in the :ref:`values`
   section in chapter 2 to determine the size of a ``Rectangle``.


4. Compile and run the following program:

   .. literalinclude :: sourcecode/sizeof_structs.cpp
      :language: cpp

   Can you explain the results? Do some web research to see if you can
   understand what you see.  Try additional *experiments* to test your
   understanding.


5. Write a function with a ``const`` parameter, and then modify it in the
   function body.  Try compiling it and note what happens on your system. 


6. Rewrite the ``increment`` function from the :ref:`modifiers` section without
   iteration using the approach introduced in the :ref:`incremental_dev`
   section.
