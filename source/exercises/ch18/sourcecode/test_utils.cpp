#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <vector>
#include <string>
#include <doctest.h>
#include "utils.h"
using namespace std;

TEST_CASE("Test split_on_spaces") {
    vector<string> tokens = split_on_spaces("4 5 +");
    CHECK(tokens.size() == 3);
    tokens = split_on_spaces("4 5 + 6 *");
    CHECK(tokens.size() == 5);
    CHECK(tokens[0] == "4");
    CHECK(tokens[1] == "5");
    CHECK(tokens[2] == "+");
}
