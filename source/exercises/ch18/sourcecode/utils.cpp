#include <string>
#include <vector>
using namespace std;

vector<string> split_on_spaces(const string& s)
{
    vector<string> tokens;
    string token;
    bool in_token = false;

    for (char ch : s) {
        if (ch != ' ') {
            // if ch not a space, add it to current token
            token += ch;
            in_token = true;
        } else if (in_token) {
        // if space and in a token, push token to tokens list 
            tokens.push_back(token);
            token.clear(); // clear token for next word
            in_token = false;
        }
    }
    // handle last token if string doesn't end with a space
    if (in_token)
        tokens.push_back(token);

    return tokens;
}
/*
See

https://blog.devgenius.io/iterators-in-c-stl-simplifying-container-operations-181ad5849b3b

for a nice introduction to C++ iterators
*/
