..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch18s00:

Chapter 18 Exercise Set 0: Chapter Review
=========================================

#. Using what you did in :ref:`ch17s00` as a model, set up automated unit
   tests for stacks, including a ``Makefile``, a ``src`` subdirectory,
   ``Stack.h``, and ``test_stacks.cpp`` files.

#. Add the following tests to ``test_stacks.cpp``:

   .. sourcecode:: cpp

       TEST_CASE("Test basic stack operations on stack of ints") {
           Stack<int> stack;
           stack.push(9);
           CHECK(stack.top() == 9);
           stack.push(11);
           CHECK(stack.top() == 11);
           stack.push(42);
           CHECK(stack.top() == 42);
           CHECK(stack.pop() == 42);
           CHECK(stack.top() == 11);
           CHECK(stack.empty() == false);
           stack.pop();
           CHECK(stack.pop() == 9);
           CHECK(stack.empty() == true);
       }

#. Add the following tests to ``test_stacks.cpp``:

   .. sourcecode:: cpp

       TEST_CASE("Test basic stack operations on stack of strings") {
           Stack<string> stack;
           stack.push("cheese");
           CHECK(stack.top() == "cheese");
           stack.push("anchovies");
           CHECK(stack.top() == "anchovies");
           stack.push("onions");
           CHECK(stack.top() == "onions");
           CHECK(stack.pop() == "onions");
           CHECK(stack.top() == "anchovies");
           CHECK(stack.empty() == false);
           stack.pop();
           CHECK(stack.pop() == "cheese");
           CHECK(stack.empty() == true);
       }

#. Write your own tests for a stack of user defined objects (``Card`` perhaps?).

#. Run your test suite with all three implementations of the Stack ADT that
   were introduced in the chapter.

#. Investigate the `stack <https://en.cppreference.com/w/cpp/container/stack>`_ 
   in the C++ standard library. How is this implementation different from
   the `NIST ADT for stack <https://xlinux.nist.gov/dads/HTML/stack.html>`_?
   How is it like it? What changes would need to be made to our doctests above
   to make them work with this version of stack?
