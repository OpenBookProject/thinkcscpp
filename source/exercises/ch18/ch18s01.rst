..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".


.. _ch18s01:

Chapter 18 Exercise Set 1: Stack Applications
=============================================

#. Write a function named ``split_on_spaces`` that takes a string as an
   argument and returns a vector of strings by separating the argument using
   spaces as the delimiter. The following doctests will make this task
   explicit:

   .. sourcecode:: cpp
   
       TEST_CASE("Test split_on_spaces") {
           vector<string> tokens = split_on_spaces("4 5 +");
           CHECK(tokens.size() == 3);
           tokens = split_on_spaces("4 5 + 6 *");
           CHECK(tokens.size() == 5);
           CHECK(tokens[0] == "4");
           CHECK(tokens[1] == "5");
           CHECK(tokens[2] == "+");
       }


#. Write a Boolean function named ``is_valid_infix_expression`` that takes a
   string containing a mathematical expression with ``+``, ``-``, ``*``,
   ``/``, and ``%`` operators, parentheses, ``()``, for grouping, and numeric
   (digits) and variable (letters) operands, and returns ``true`` if the
   expression is valid and ``false`` if it is not.

   .. sourcecode:: cpp

       TEST_CASE("Test is_valid_infix_expression") {
           string expr = "(4 + a) / c"
           CHECK(is_valid_infix_expression(expr) == true);
           string expr = "(x * ((5 - d) + c) - y) * 2"
           CHECK(is_valid_infix_expression(expr) == true);
           string expr = "(x * ((5 - d) + c - y) * 2"
           CHECK(is_valid_infix_expression(expr) == false);
       }


#. Write a Boolean function named ``has_matching_tags`` that takes a
   string containing HTML source and returns ``true`` if each open tag has
   a corresponding closing tag in the right place. 


#. Investigate the famous `Towers of Hanoi
   <https://en.wikipedia.org/wiki/Tower_of_Hanoi>`_ puzzle, write a C++
   program to print the steps to move a tower of size ``n`` using
   `recursion <https://en.wikipedia.org/wiki/Recursion>`_ (introduced in
   chapter 4 :ref:`recursion_section`), and explain how this illustrates the
   relationship between stacks and recursion.
