#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<float *> fpv = {new float[1], new float[1], new float[1]};    
    for (int i = 0; i < 3; i++) {
        float *fp = fpv[i];
        *fp = 2 * i + 1;
    }
    for (int i = 0; i < 3; i++) {
        cout << *fpv[i];
        delete[] fpv[i];
    }
    cout << endl;
    return 0;
}
