#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<double> a = {1e-1, 1e0, 1e1};
    double *dp = a.data() + 2;
    cout << a[1] - *dp << endl;
    return 0;
}
