#include <iostream>
#include <vector>
using namespace std;

void swap(int* ip1, int* ip2) {
    int n = *ip1;
    int m = *ip2;
} 

int main() {
    vector<int> a = {3, 2, 1};
    swap(&a[0], &a[1]);
    swap(&a[2], &a[0]);
    cout << a[0] << ' ' << a[1] <<  ' ' << a[2] << endl;
    return 0;
}
