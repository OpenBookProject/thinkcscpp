..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch19s00:

Chapter 19 Exercise Set 0: Chapter Review
=========================================

#. Create a full set of `unit tests
   <https://en.wikipedia.org/wiki/Unit_testing>`_ using
   `doctest <https://github.com/doctest/doctest>`_ in a file named
   ``test_queues.cpp`` as we have been doing since :ref:`ch10s02`. Then
   add a ``Makefile`` as we have been doing since :ref:`ch13s01`.

#. Write an implementation of ``Queue`` that *wraps* ``LinkedList`` (*hint* 
   use :ref:`stacks_wrapping_linked_lists` as an example to help you.)
   Test your new implementation using the same test suite you created in the
   ``test_queues.cpp`` file in the previous exercise.

#. Read `Big O Notation Tutorial – A Guide to Big O Analysis
   <https://www.geeksforgeeks.org/analysis-algorithms-big-o-analysis/>`_.
