#include <iostream>
using namespace std;

int main() {
    int* ia[3];

    for (int i = 0; i < 3; i++) {
        ia[i] = new int[i + 1];
        for (int j = 0; j < i + 1; j++)
            ia[i][j] = 10 * i + j + 2;
    }
    cout << ia[0][0] << endl;
    cout << ia[1][0] << ' ' << ia[1][1] << endl;
    cout << ia[2][0] << ' ' << ia[2][1] << ' ' << ia[2][2] << endl; 
    for (int i = 0; i < 3; i++)
        delete[] ia[i];
    return 0;
}
