#include <string>
using namespace std;

struct Time
{
    int seconds;

    Time();
    Time(int);

    string to_string() const;
};
