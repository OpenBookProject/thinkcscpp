#include <string>
#include "Time.h"

Time::Time() {
    seconds = 0;
}

Time::Time(int s) {
    seconds = s;
}

string Time::to_string() const {
    int h = seconds / 3600;
    int m = (seconds % 3600) / 60;
    int s = seconds % 60;

    string time_str = std::to_string(h) + ":"; 
    time_str += (m > 9) ? std::to_string(m) : "0" + std::to_string(m);
    time_str += ":"; 
    time_str += (s > 9) ? std::to_string(s) : "0" + std::to_string(s);

    return time_str;
}
