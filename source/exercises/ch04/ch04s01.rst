..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch04s01:

Chapter 4 Exercise Set 1: CPE Practice
=======================================

1. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe01.cpp
      :language: cpp

   .. Solution: 7


2. What is the final value of the ``c`` variable?

   .. sourcecode:: cpp

       int a = 4, b = 3, c = 2;
       if (a > 0) {
           b -= 4;
           if (b > 0) {
               if (c > 0)
                   c++;
               if (c <= 3)
                   c--;
           }
       if (b < 0)
           a--;
       }
       c = a + b + c;

   .. Solution: 4


3. What is the value of the ``n`` variable?

   .. sourcecode:: cpp

       int n = 1 % 2 + 4 % 2 

   .. Solution: 1


4. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe04.cpp
      :language: cpp

   .. Solution: 6


5. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe05.cpp
      :language: cpp

   .. Solution: 1012 


6. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe06.cpp
      :language: cpp

   .. Solution: 1120 


7. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe07.cpp
      :language: cpp

   .. Solution: 2911110


8. What is the output of the following program?

   .. literalinclude :: sourcecode/cpe08.cpp
      :language: cpp

   .. Solution: 36 


9. What is the value of the ``n`` variable?

   .. sourcecode:: cpp

       int n = (3 << 2) + (4 >> 1);

   .. Solution: 14


10. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe10.cpp
       :language: cpp

    .. Solution: 671


11. Given that the `ASCII <https://en.wikipedia.org/wiki/ASCII>`_ value of
    ``'a'`` is ``97``, what is the output of the following program?

    .. literalinclude :: sourcecode/cpe11.cpp
       :language: cpp

    .. Solution: 158


12. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe12.cpp
       :language: cpp

    .. Solution: no


13. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe13.cpp
       :language: cpp

    .. Solution: mustard and ice cream 


14. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe14.cpp
       :language: cpp

    .. Solution: 4


15. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe15.cpp
       :language: cpp

    .. Solution: true


16. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe16.cpp
       :language: cpp

    .. Solution: 2


17. What is the output of the following program?

    .. literalinclude :: sourcecode/cpe17.cpp
       :language: cpp

    .. Solution: 4


