#include <iostream>
using namespace std;

int main() {
    int a = 0, b = a++, c = --a;

    if (a > 0)
        b++;
    else
        c++;
    if (c == 0)
        c--;
    else
        c++;

    cout << a + b + c << endl; 

    return 0;
}
