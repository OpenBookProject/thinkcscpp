..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch01s00:

Chapter 1 Exercise Set 0: Chapter Review
========================================

#. **Syntax and Semantics**

   Do a web search (`Wikipedia <https://en.wikipedia.org>`_ is your
   best place to start) and define the following terms in
   *your own words*:

    * alphabet
    * lexeme
    * syntax
    * semantics

   Write an English sentence with understandable semantics but
   incorrect syntax. Write another English sentence which has
   correct syntax but has semantic errors.


#. **Introducing the C++ Build Process**

   The journey from C++ source file to compiled machine language
   executable goes through four stages, each controlled by a
   separate program.  Do a web search on the following programs,
   and summarize what each of them do in your own words.

    * Preprocessor
    * Compiler
    * Assembler
    * Linker
