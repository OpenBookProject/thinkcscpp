#include <iostream>
#include <vector>
#include <string>
#include "Cards.h"
using namespace std;

string suit_string(Suit suit) {
    switch (suit) {
        case CLUBS: return "Clubs";
        case DIAMONDS: return "Diamonds";
        case HEARTS: return "Hearts";
        case SPADES: return "Spades";
        default: return "Not a valid suit";
    }
}

string rank_string(Rank rank) {
    switch (rank) {
        case JOKER: return "Joker";
        case TWO: return "2";
        case THREE: return "3";
        case FOUR: return "4";
        case FIVE: return "5";
        case SIX: return "6";
        case SEVEN: return "7";
        case EIGHT: return "8";
        case NINE: return "9";
        case TEN: return "10";
        case JACK: return "Jack";
        case QUEEN: return "Queen";
        case KING: return "King";
        case ACE: return "ACE";
        default: return "Not a valid rank";
    }
}

Card::Card()
{
    suit = NONE;
    rank = JOKER;
}

Card::Card(Suit s, Rank r)
{
    suit = s; rank = r;
}

string Card::to_string() const
{
    if (rank == JOKER) return "Joker";
    return rank_string(rank) + " of " + suit_string(suit);
}
