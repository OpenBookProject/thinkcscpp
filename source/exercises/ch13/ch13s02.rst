..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _ch13s02:

Chapter 13 Exercise Set 2: It's War! 
====================================

We will be modifying the ``Card`` objects we created in the
:ref:`vectors_of_objects_chapter` chapter, using the same ``Cards.h`` and
``Cards.cpp`` files. The new ``Deck`` objects will be added there too.

#. Rewrite the ``find_card`` function from :ref:`searching` as a ``Deck``
   member function that has a ``Card`` parameter. 

#. Reread the discussion in :ref:`random_numbers` and use that to write
   a function named ``random_between``. Create a header file, ``random.h``,
   with the following:

   .. literalinclude:: sourcecode/random.h
      :language: cpp

   Then create a ``random.cpp`` that has the body of the function. The
   parameters ``l`` and ``h`` represent the lower and upper ends of a range
   or integers.  Your function should return random integers between ``l``
   and ``h`` inclusive. *Note*: Your function should not concern itself with
   the need to seed ``rand`` by calling ``srand``. Call ``srand`` in the
   ``main`` function.

   Write a ``test_random.cpp`` that has ``#include "random.h"`` at the top and
   has several calls to ``random_between`` in its ``main`` function designed to
   test it. Try simulating the roll of a 6 sided die. Don't forget to call
   ``srand`` so that you get a different sequence of numbers each time you run
   your program.

   Handle the case where ``l`` is greater than ``h`` by swapping ``l`` and
   ``h`` so that both ``random_between(3, 6)`` and ``random_between(6, 3)``
   return an integer between 3 and 6 inclusive.

#. Add a modifier member function to the ``Deck`` structure named
   ``swap_cards`` that takes integers as arguments (the indices of the two
   cards) and swaps the cards at these indices.

#. Add a member function to the ``Deck`` structure with prototype: 

   .. sourcecode:: cpp

       int find_lowest_between(int l, int h);

   that returns the index of the lowest card in the deck between index ``l``
   and index ``h`` inclusive.

#. Add a modifier member function to the ``Deck`` structure named ``sort``
   that uses the selection sort algorithm sketched out in the
   :ref:`selection_sort` section to put the deck's cards into sorted order.

#. Complete the ``merge`` function sketched out in the :ref:`merge_sort`
   section. Then use it to complete the first version of ``merge_sort``
   described in the section.
