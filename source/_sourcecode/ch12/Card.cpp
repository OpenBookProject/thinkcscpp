#include <iostream>
#include <vector>
#include <string>
#include "Card.h"
using namespace std;

Card::Card()
{
    suit = 0; rank = 0;
}

Card::Card(int s, int r)
{
    suit = s; rank = r;
}

string Card::to_string() const
{
    vector<string> suit_strings = {
        "None", "Clubs", "Diamonds", "Hearts", "Spades"
    };
    vector<string> rank_strings = {
        "Joker", "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10",
        "Jack", "Queen", "King"
    };
    if (rank == 0)
        return rank_strings[rank];
    return rank_strings[rank] + " of " + suit_strings[suit];
}

bool Card::operator==(const Card& c2) const
{
    return (suit == c2.suit && rank == c2.rank);
}

bool Card::operator>(const Card& c2) const
{
    // first check the suit_strings
    if (suit > c2.suit) return true;
    if (suit < c2.suit) return false;

    // if suit_strings are equal, check rank_strings, starting with Aces
    if (rank == 1 && c2.rank != 1) return true; // Aces beat Kings
    if (rank == 1 && c2.rank == 1) return false;
    if (rank > c2.rank) return true;
    if (rank < c2.rank) return false;

    // if rank_strings are equal too, 1st card is not greater than the 2nd 
    return false;
}

bool Card::operator>=(const Card& c2) const
{
    return this->operator>(c2) || this->operator==(c2);
}

bool Card::operator<=(const Card& c2) const
{
    return !(this->operator>(c2));
}

bool Card::operator<(const Card& c2) const
{
    return !(this->operator>(c2)) && !(this->operator==(c2));
}

bool Card::operator!=(const Card& c2) const
{
    return !(this->operator==(c2));
}

ostream& operator<<(ostream& out, Card c) {
    out << c.to_string();
    return out;
}
