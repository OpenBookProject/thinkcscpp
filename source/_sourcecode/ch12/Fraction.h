#include <iostream>
#include <string>
using namespace std;

struct Fraction 
{
    int numerator, denominator;

    // constructors
    Fraction();
    Fraction(int);
    Fraction(int, int);
    Fraction(string);

    // member functions
    string to_string() const;
    void set_numerator_denominator(int, int);
    bool operator==(const Fraction&) const;
    bool operator>(const Fraction&) const;
    bool operator<(const Fraction&) const;
    bool operator>=(const Fraction&) const;
    bool operator<=(const Fraction&) const;
    bool operator!=(const Fraction&) const;
};

ostream& operator<<(ostream&, Fraction);

int gcd(int, int);
