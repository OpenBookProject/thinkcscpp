#include <iostream>
#include <string>
using namespace std;

struct Card
{
    int suit, rank;

    // constructors
    Card();
    Card(int s, int r);

    // member functions
    string to_string() const;
    bool operator==(const Card& c2) const;
    bool operator>(const Card& c2) const;
    bool operator<(const Card& c2) const;
    bool operator>=(const Card& c2) const;
    bool operator<=(const Card& c2) const;
    bool operator!=(const Card& c2) const;
};

ostream& operator<<(ostream&, Card);
