#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <iostream>
#include <string>
#include "Card.h"
using namespace std;

TEST_CASE("Test can create Cards") {
    Card c1;
    CHECK(c1.suit == 0);
    CHECK(c1.rank == 0);
    Card c2(3, 4);
    CHECK(c2.suit == 3);
    CHECK(c2.rank == 4);
}

TEST_CASE("Test can render Cards") {
    Card c1(3, 12);
    CHECK(c1.to_string() == "Queen of Hearts");
    Card c2(2, 10);
    CHECK(c2.to_string() == "10 of Diamonds");
    Card c3;
    CHECK(c3.to_string() == "Joker");
}

TEST_CASE("Test comparison of Cards") {
    Card c1(2, 9);
    Card c2(1, 9);
    Card c3(1, 10);
    Card c4(1, 10);
    CHECK((c3 <= c2) == false);
    CHECK((c2 < c1) == true);
    CHECK((c3 == c4) == true);
    CHECK((c1 >= c3) == true);
    CHECK((c3 != c4) == false);
}

TEST_CASE("Test comparisons with Aces") {
    Card c1(4, 1);
    Card c2(4, 2);
    Card c3(4, 13);
    CHECK((c1 > c2) == true);
    CHECK((c1 > c3) == true);
}
