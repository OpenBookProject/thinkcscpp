public class UnorderedBools {
    public static void main(String[] args) {
        boolean p = true;
        boolean q = false;

        if (p > q) {
            System.out.println("Truth prevails over falsehood.");
        }
    }
}
