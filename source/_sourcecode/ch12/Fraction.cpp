#include <iostream>
#include <vector>
#include <string>
#include "Fraction.h"
using namespace std;

Fraction::Fraction()
{
    numerator = 0; denominator = 1;
}

Fraction::Fraction(int n)
{
    numerator = n; denominator = 1;
}

Fraction::Fraction(int n, int d)
{
    set_numerator_denominator(n, d);
}

Fraction::Fraction(string fstring)
{
    int n, d;
    int bpos = fstring.find('/');
    if (bpos == -1) {
        n = stoi(fstring);
        d = 1;
    } else {
        n = stoi(fstring.substr(0, bpos));
        d = stoi(fstring.substr(bpos+1, fstring.length() - bpos));
    }
    set_numerator_denominator(n, d);
}

string Fraction::to_string() const
{
    if (denominator == 1) return ::to_string(numerator);
    return ::to_string(numerator) + "/" + ::to_string(denominator);
}

void Fraction::set_numerator_denominator(int n, int d) {
    int divisor = gcd(n, d);
    numerator = n / divisor;
    denominator = d / divisor;
}

bool Fraction::operator==(const Fraction& f2) const
{
    return (numerator == f2.numerator && denominator == f2.denominator);
}

bool Fraction::operator!=(const Fraction& f2) const
{
    return !(this->operator==(f2));
}

bool Fraction::operator>(const Fraction& f2) const
{
    return numerator * f2.denominator > f2.numerator * denominator;
}

bool Fraction::operator<=(const Fraction& f2) const
{
    return !(this->operator>(f2));
}

bool Fraction::operator<(const Fraction& f2) const
{
    return !(this->operator>(f2)) && !(this->operator==(f2));
}

bool Fraction::operator>=(const Fraction& f2) const
{
    return !(this->operator>(f2)) || !(this->operator==(f2));
}

ostream& operator<<(ostream& out, Fraction f) {
    out << f.to_string();
    return out;
}

int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a % b);
}
