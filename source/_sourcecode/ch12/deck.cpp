#include <iostream>
#include <vector>
#include <string>
#include "Card.h"
using namespace std;

vector<Card> build_deck()
{
    vector<Card> deck(52);
    int i = 0;
    for (int suit = 0; suit <= 3; suit++) {
        for (int rank = 1; rank <= 13; rank++) {
            deck[i].suit = suit;
            deck[i].rank = rank;
            i++;
        }
    }
    return deck;
}

void print_deck(const vector<Card>& deck)
{
    for (int i = 0; i < deck.size(); i++) {
        cout << deck[i].toString() << endl;
    }
}

int find_card(const Card& card, const vector<Card>& deck)
{
    for (int i = 0; i < deck.size(); i++) {
        if (deck[i].equals(card)) return i;
    }
    return -1;
}

int bin_search(const Card& card, const vector<Card>& deck, int l, int h) {
    cout << l << ", " << h << endl;

    if (h < l) return -1;

    int m = (l + h) / 2;

    if (deck[m].equals(card)) return m;

    if (deck[m].is_greater(card))
        return bin_search(card, deck, l, m-1);
    else
        return bin_search(card, deck, m+1, h);
}

int main()
{
    vector<Card> deck = build_deck();
    // print_deck(deck);
    int index = find_card(deck[42], deck);
    cout << "Card was found at index " << index << "." << endl;

    index = bin_search(deck[23], deck, 0, 51);
    cout << "Card found at index " <<  index << endl;

    Card fake_card(1, 15);
    index = bin_search(fake_card, deck, 0, 51);
    cout << "Card found at index " <<  index << endl;

    return 0;
}
