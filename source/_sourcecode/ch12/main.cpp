#include <iostream>
#include <vector>
#include <string>
#include "Card.h"
using namespace std;

int main()
{
    Card c1(2, 11);
    Card c2(1, 11);

    if (c1.is_greater(c2)) {
        cout << c1.to_string() << " is greater than " << c2.to_string() << endl;
    }

    Card ace_of_spades(3, 1);
    vector<Card> deck(52, ace_of_spades);
    cout << deck[0].to_string() << endl;

    return 0;
}
