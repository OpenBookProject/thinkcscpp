#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iomanip>
using namespace std;

vector<int> random_vector(int n, int min, int max)
{
    vector<int> vec(n);
    srand(time(NULL));
    for (int i = 0; i < vec.size(); i++) {
        vec[i] = min + (rand() % (max + 1));
    }
    return vec;
}

void print_vector(const vector<int>& vec)
{
    for (int i = 0; i < vec.size(); i++) {
        cout << vec[i] << " ";
    }
    cout << endl;
}

int how_many(const vector<int>& vec, int value)
{
    int count = 0;
    for (int i = 0; i < vec.size(); i++) {
        if (vec[i] == value) count++;
    }
    return count;
}

int main()
{
    int num_values = 100000;
    int lower_bound = 1;
    int upper_bound = 10;
    vector<int> v = random_vector(num_values, lower_bound, upper_bound); 
    cout << "value how_many" << endl;
    for (int i = lower_bound; i <= upper_bound; i++)
        cout << left << setw(6) << i << how_many(v, i) << endl;
    return 0;
}
