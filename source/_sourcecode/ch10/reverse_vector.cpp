#include <iostream>
#include <vector>
using namespace std;

void print_numbers(const vector<int> v) {
   if (!v.empty()) {
       auto itr = v.begin();
       cout << *itr;
       for (itr++; itr < v.end(); itr++)
           cout << ' ' <<  *itr;
   }
   cout << endl;
}
int main() {
    vector<int> numbers = {2, 4, 5, 7, 11, 13, 16, 19, 21, 22};
    vector<int> reversed;
    while (!numbers.empty()) {
        reversed.push_back(numbers.back());
        numbers.pop_back();
    }
    cout << "Reversed numbers: ";
    print_numbers(reversed);
    return 0;
}
