#include <iostream>
#include <vector>
using namespace std;

void print_numbers(const vector<int> v) {
   if (!v.empty()) {
       auto itr = v.begin();
       cout << *itr;
       for (itr++; itr < v.end(); itr++)
           cout << ' ' <<  *itr;
   }
   cout << endl;
}
int main() {
    vector<int> numbers = {13, 21, 4, 16, 7, 22, 5, 19, 2, 11};
    int pos;
    cout << "Unsorted numbers: ";
    print_numbers(numbers);
    for (int num_sorted = 0; num_sorted < numbers.size(); num_sorted++) {
        int num = numbers.back();
        numbers.pop_back();
        for (pos = 0; num > numbers[pos] && pos <= num_sorted; pos++);
        numbers.insert(numbers.begin() + pos, num);
        cout << "After iteration " << num_sorted + 1 << ": ";
        print_numbers(numbers);
    }
    cout << "Sorted numbers: ";
    print_numbers(numbers);
    return 0;
}
