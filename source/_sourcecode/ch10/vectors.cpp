#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> count(4, 2);
    count[0] = 7;
    count[1] = count[0] * 2; 
    count[2]++;
    count[3] -= 60;

    for (int i = 0; i < count.size(); i++) {
        cout << "count[" << i << "] is " << count[i] << endl;
    }

    vector<int> copy = count;
    cout << "The size of copy is " << copy.size() << endl;

    return 0;
}
