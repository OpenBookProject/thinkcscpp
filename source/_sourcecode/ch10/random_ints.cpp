#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;

int main()
{
    int upper_bound = 101;

    cout << RAND_MAX << endl; 
    cout << "Let's generate 10 random numbers between 1 and 100." << endl;

    for (int i = 1; i < 11; i++) {
        cout << "Random number " << i << ": " << rand() % upper_bound << endl;
    }

    cout << "Let's seed rand so our numbers differ between runs." << endl;
    srand((unsigned) time(NULL));

    for (int i = 1; i < 11; i++) {
        cout << "Random number " << i << ": " << rand() % 101 << endl;
    }

    return 0;
}
