#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> square_numbers(10);

    // Initialize square_numbers vector
    for (int i = 0; i < square_numbers.size(); i++)
        square_numbers[i] = i * i;

    // Print the square numbers with indexing
    for (int i = 0; i < square_numbers.size(); i++)
        cout << square_numbers[i] << ' ';
    cout << endl;

    // Print the square numbers with pointers 
    int* ip = square_numbers.data();
    for (int i = 0; i < square_numbers.size(); i++)
        cout << *ip++ << ' ';
    cout << endl;

    // Print the square numbers with value iterator 
    for (auto item : square_numbers) {
        cout << item << ' ';
    }
    cout << endl;

    // Print the square numbers with pointer iterator
    for (auto itr = square_numbers.begin(); itr < square_numbers.end(); itr++) {
        cout << *itr << ' ';
    }
    cout << endl;

    return 0;
}
