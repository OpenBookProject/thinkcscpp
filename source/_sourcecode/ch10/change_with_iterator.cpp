#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> numbers(10);
    // Initialize numbers vector with even numbers
    for (int i = 0; i < numbers.size(); i++)
        numbers[i] = 2 * i;
    // Print the even numbers with value iterator 
    for (auto number: numbers) {
        cout << number << ' ';
    }
    cout << endl;
    // Change the even numbers to odd numbers with pointer iterator
    for (auto itr = numbers.begin(); itr < numbers.end(); itr++) {
        *itr += 1;
    }
    // Now print the odd numbers with value iterator 
    for (auto number: numbers) {
        cout << number << ' ';
    }
    cout << endl;
    return 0;
}
