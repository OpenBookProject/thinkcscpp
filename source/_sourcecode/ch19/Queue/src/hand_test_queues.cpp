#include <iostream>
#include <string>
#include "Queue.h"
using namespace std;

int main()
{
    Queue<string>* q = new Queue<string>;
    cout << "Calling q->empty() returns: " << q->empty() << endl;
    cout << "New Jersey queues up..." << endl;
    q->insert("New Jersey");
    cout << "Calling q->empty() returns: " << q->empty() << endl;
    cout << "Maryland queues up..." << endl;
    q->insert("Maryland");
    cout << "Virginia queues up..." << endl;
    q->insert("Virginia");
    cout << q->remove() << " finishes its business..." << endl;
    cout << q->remove() << " finishes its business..." << endl;
    cout << "DC queues up..." << endl;
    q->insert("DC");
    cout << q->remove() << " finishes its business..." << endl;

    return 0;
}
