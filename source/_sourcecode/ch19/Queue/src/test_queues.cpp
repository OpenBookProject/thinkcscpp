#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <string>
#include "Queue.h"
using namespace std;

TEST_CASE("Test can create empty Queue") {
    Queue<string> q;
    CHECK(q.empty() == true);
}

TEST_CASE("Test can insert and remove items in Queue") {
    Queue<int> q;
    q.insert(1);
    CHECK(q.empty() == false);
    q.insert(2);
    q.insert(3);
    q.insert(4);
    CHECK(q.remove() == 1);
    CHECK(q.remove() == 2);
    CHECK(q.remove() == 3);
    CHECK(q.remove() == 4);
    CHECK(q.empty() == true);
}
