#include <iostream>
#include "Complex.h"
using namespace std;

int main()
{
    Complex c1(1, 0.8, POLAR);

    cout << c1.str_polar() << endl;

    return 0;
}
