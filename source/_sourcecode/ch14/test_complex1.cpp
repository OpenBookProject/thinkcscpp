#include <iostream>
#include "Complex.h"
using namespace std;

int main()
{
    Complex c(2, 3);

    cout << c.str_cartesian() << endl;
    cout << c.str_polar() << endl;

    return 0;
}
