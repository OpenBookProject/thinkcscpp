#include <iostream>
#include "Complex.h"
using namespace std;

int main()
{
    Complex c1(2, 3);
    Complex c2(4, 7);

    cout << c1.str_cartesian() << endl;
    cout << c2.str_cartesian() << endl;

    Complex sum = c1 + c2;
    cout << sum.str_cartesian() << endl;

    return 0;
}
