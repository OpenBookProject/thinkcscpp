#include <string>
using namespace std;

struct Node
{
    int cargo;
    Node* next;

    // constructors
    Node();
    Node(int);
    Node(int, Node*);
};

class LinkedList
{
    int num_nodes;
    Node* head;

public:
    LinkedList();

    // member functions
    bool empty();
    int size();
    string to_string();

    // modifiers
    void insert_at_front(int);
    void insert_at_end(int);
    void insert_in_order(int);
    int remove_from_front();
    int remove_from_end();
    /*
    void insert_at_pos(int, int);
    void remove_at_pos(int, int);
    bool find(int);
    */
};
