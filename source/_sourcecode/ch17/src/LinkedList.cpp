#include <iostream>
#include <string>
#include "LinkedList.h"
using namespace std;

Node::Node()
{
    cargo = 0;
    next = nullptr;
}

Node::Node(int cargo)
{
    this->cargo = cargo;
    next = nullptr;
}

Node::Node(int cargo, Node* next)
{
    this->cargo = cargo;
    this->next = next;
}

LinkedList::LinkedList()
{
    num_nodes = 0;
    head = nullptr;
}

bool LinkedList::empty()
{
    return num_nodes == 0;
}

int LinkedList::size()
{
    return num_nodes;
}

void LinkedList::insert_at_front(int val)
{
    Node* node = new Node(val, head);
    head = node;
    num_nodes++;
}

void LinkedList::insert_at_end(int val)
{
    Node* node = new Node(val);
    Node* nptr = head;
    if (nptr == nullptr)
        head = node;
    else {
        while (nptr->next != nullptr)
            nptr = nptr->next;
        nptr->next = node;
    }
    num_nodes++;
}

void LinkedList::insert_in_order(int val)
{
    Node* node = new Node(val);
    num_nodes++;
    // Handle empty list case
    if (head == nullptr) {
        head = node;
        return;
    }
    // Handle insert in front case
    if (node->cargo < head->cargo) {
        node->next = head;
        head = node;
        return;
    }
    // Handle insert after first node case
    Node* prevnd = head;
    Node* nextnd = prevnd->next;
    // Handle insert after single node list case
    if (nextnd == nullptr) {
        head->next = node;
        return;
    }
    // Find insertion point with leading and trailing node pointers
    while (nextnd->next != nullptr && node->cargo > nextnd->cargo) {
        prevnd = nextnd;
        nextnd = nextnd->next;
    }
    // Handle end of list case
    if (nextnd->next == nullptr && node->cargo > nextnd->cargo) {
        nextnd->next = node;
        return;
    }
    // Handle insert between two nodes case
    prevnd->next = node;
    node->next = nextnd;
}

int LinkedList::remove_from_front()
{
    if (empty())
        throw runtime_error("Can't remove from empty list!");
    Node* front = head;
    head = head->next;
    num_nodes--;
    int val = front->cargo;
    delete front;
    return val;
}

int LinkedList::remove_from_end()
{
    if (empty())
        throw runtime_error("Can't remove from empty list!");
    int val;
    Node* nodeptr = head;
    num_nodes--;
    // Handle singlton list case
    if (nodeptr->next == nullptr) {
        val = nodeptr->cargo;
        head = nullptr;
        delete nodeptr;
        return val;
    }
    // Find end of list
    while (nodeptr->next->next != nullptr)
        nodeptr = nodeptr->next;
    val = nodeptr->next->cargo;
    Node* last = nodeptr->next;
    nodeptr->next = nullptr;
    delete last;
    return val;
}

string LinkedList::to_string()
{
    Node* node = head;
    string s = "";
    while (node != nullptr) {
        s += std::to_string(node->cargo);
        node = node->next;
        if (node != nullptr)
            s += ", ";
    }
    return s;
}
/*
    int remove_from_end();
    void insert_at_pos(int, int);
    void remove_at_pos(int, int);
    bool find(int);
*/

