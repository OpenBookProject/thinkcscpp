#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <string>
#include "LinkedList.h"
using namespace std;

TEST_CASE("Test can create and link Nodes") {
    Node node;
    CHECK(node.cargo == 0);
    CHECK(node.next == nullptr);
    Node node2(42);
    CHECK(node2.cargo == 42);
    CHECK(node2.next == nullptr);
    Node node3(1, &node2);
    CHECK(node3.cargo == 1);
    CHECK(node3.next == &node2);
}

TEST_CASE("Test can create and insert nodes in LinkedList") {
    LinkedList numlist;
    CHECK(numlist.size() == 0);
    CHECK(numlist.empty() == true);
    numlist.insert_at_front(11);
    CHECK(numlist.size() == 1);
    CHECK(numlist.empty() == false);
    numlist.insert_at_front(15);
    CHECK(numlist.size() == 2);
    numlist.insert_at_front(42);
    CHECK(numlist.size() == 3);
    CHECK(numlist.to_string() == "42, 15, 11");
}

TEST_CASE("Test can insert nodes at front and end of LinkedList") {
    LinkedList numlist;
    numlist.insert_at_front(2);
    numlist.insert_at_front(3);
    numlist.insert_at_front(4);
    CHECK(numlist.to_string() == "4, 3, 2");
    numlist.insert_at_end(1);
    CHECK(numlist.to_string() == "4, 3, 2, 1");
    numlist.insert_at_end(0);
    CHECK(numlist.to_string() == "4, 3, 2, 1, 0");
}

TEST_CASE("Test can insert nodes in ordered LinkedList") {
    LinkedList numlist;
    numlist.insert_in_order(6);
    CHECK(numlist.to_string() == "6");
    numlist.insert_in_order(3);
    CHECK(numlist.to_string() == "3, 6");
    numlist.insert_in_order(9);
    CHECK(numlist.to_string() == "3, 6, 9");
    numlist.insert_in_order(1);
    CHECK(numlist.to_string() == "1, 3, 6, 9");
    numlist.insert_in_order(5);
    CHECK(numlist.to_string() == "1, 3, 5, 6, 9");
    numlist.insert_in_order(8);
    CHECK(numlist.to_string() == "1, 3, 5, 6, 8, 9");
}

TEST_CASE("Test can remove nodes from front and rear of LinkedList") {
    LinkedList numlist;
    for (int i = 7; i > 0; i--)
        numlist.insert_at_front(i);
    CHECK(numlist.to_string() == "1, 2, 3, 4, 5, 6, 7");
    CHECK(numlist.remove_from_front() == 1);
    CHECK(numlist.remove_from_end() == 7);
    CHECK(numlist.to_string() == "2, 3, 4, 5, 6");
}
