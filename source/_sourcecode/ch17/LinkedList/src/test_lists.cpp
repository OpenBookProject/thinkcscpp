#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <string>
#include "LinkedList.h"
using namespace std;

TEST_CASE("Test basic list of strings operations") {
    LinkedList<string> toppings;
    CHECK(toppings.length() == 0);
    toppings.insert_at_front("cheese");
    toppings.insert_at_front("anchovies");
    toppings.insert_at_front("onions");
    CHECK(toppings.length() == 3);
    CHECK(toppings.remove_from_front() == "onions");
    CHECK(toppings.length() == 2);
}

TEST_CASE("Test insert and remove at end with list of ints") {
    LinkedList<int> nums;
    nums.insert_at_front(7);
    nums.insert_at_front(9);
    nums.insert_at_front(11);
    CHECK(nums.length() == 3);
    nums.insert_at_end(5);
    CHECK(nums.length() == 4);
    CHECK(nums.remove_from_end() == 5);
    CHECK(nums.length() == 3);
}

TEST_CASE("Test get_item_at") {
    LinkedList<int> nums;
    nums.insert_at_front(7);
    nums.insert_at_front(9);
    nums.insert_at_front(11);
    CHECK(nums.get_item_at(1) == 11);
    CHECK(nums.get_item_at(2) == 9);
    CHECK(nums.get_item_at(3) == 7);
}

TEST_CASE("Test insert_item_at") {
    LinkedList<int> nums;
    nums.insert_item_at(7, 0);
    nums.insert_item_at(11, 1);
    CHECK(nums.get_item_at(1) == 7);
    CHECK(nums.get_item_at(2) == 11);
    nums.insert_item_at(9, 1);
    CHECK(nums.get_item_at(1) == 7);
    CHECK(nums.get_item_at(2) == 9);
    CHECK(nums.get_item_at(3) == 11);
}

TEST_CASE("Test remove_item_at") {
    LinkedList<int> nums;
    nums.insert_at_front(5);
    nums.insert_at_front(4);
    nums.insert_at_front(3);
    nums.insert_at_front(2);
    nums.insert_at_front(1);
    CHECK(nums.length() == 5);
    CHECK(nums.get_item_at(1) == 1);
    CHECK(nums.get_item_at(2) == 2);
    CHECK(nums.get_item_at(3) == 3);
    CHECK(nums.get_item_at(4) == 4);
    CHECK(nums.get_item_at(5) == 5);
    CHECK(nums.remove_item_at(3) == 3);
    CHECK(nums.length() == 4);
    CHECK(nums.get_item_at(1) == 1);
    CHECK(nums.get_item_at(2) == 2);
    CHECK(nums.get_item_at(3) == 4);
    CHECK(nums.get_item_at(4) == 5);
    CHECK(nums.get_item_at(3) == 4);
}
