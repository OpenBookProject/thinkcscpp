#include <iostream>
using namespace std;

int main()
{
    int x = 100; 
    int y = -100;

    while (x > 0 && y < 0) {
        x -= 2; 
        y += 5; 
       
        cout << "x: " << x << endl;
        cout << "y: " << y << endl;
        cout << "condition: " << (x > 0 && y < 0) << endl;
   }

   return 0;
}
