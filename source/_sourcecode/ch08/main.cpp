#include <iostream>
#include "Time.h"
using namespace std;


int main()
{
    Time currentTime(9, 14, 30);
    Time doneTime = currentTime.add(Time(3, 15, 0.0));
    cout << "The current time is: ";
    currentTime.print();
    cout << " o'clock." << endl;

    if (doneTime.after(currentTime)) {
        cout << "The bread will be done after it starts." << endl;
        cout << "At ";
        doneTime.print();
        cout << " o'clock." << endl;
    }

    return 0;
}
