#include <iostream>
#include "Time.h"
using namespace std;

Time::Time(double secs)
{
    cout << "Time constructor number 1 called" << endl;
    hour = int(secs / 3600);
    secs -= hour * 3600;
    minute = int(secs / 60);
    secs -= minute * 60;
    second = secs;
}

Time::Time(int h, int m, double s)
{
    cout << "Time constructor number 2 called" << endl;
    hour = h; minute = m; second = s;
}

Time::Time(int h, int m, int s)
{
    cout << "Time constructor number 3 called" << endl;
    hour = h; minute = m; second = (double)s;
}

void Time::increment(double secs)
{
    secs += convert_to_seconds();
    return Time(convert_to_seconds() + t2.convert_to_seconds());
    hour = int(secs / 3600);
    secs -= hour * 3600;
    minute = int(secs / 60);
    secs -= minute * 60;
    second = secs;
}

void Time::print() const
{
    cout << hour << ":" << minute << ":" << second;
}

bool Time::after(const Time& t2) const
{
    if (hour > t2.hour) return true;
    if (hour < t2.hour) return false;

    if (minute > t2.minute) return true;
    if (minute < t2.minute) return false;

    return (second > t2.second);
}

Time Time::add(const Time& t2) const
{
    return Time(convert_to_seconds() + t2.convert_to_seconds());
}

double Time::convert_to_seconds() const
{
    return (hour * 60 + minute) * 60 + second;
}
