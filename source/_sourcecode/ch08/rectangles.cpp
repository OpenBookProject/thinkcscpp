#include <iostream>
#include <math.h>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point corner;
    double width, height;
};

void print_point(Point p)
{
    cout << "(" << p.x << ", " << p.y << ")" << endl;
}

Point find_center(Rectangle& box)
{
    double x = box.corner.x + box.width/2;
    double y = box.corner.y + box.height/2;
    Point result = {x, y};
    return result;
}

int main()
{
    Rectangle rect = {{0, 0}, 100, 200}; 
    Point center = find_center(rect); 
    print_point(center);

    return 0;
}
