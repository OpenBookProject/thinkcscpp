#include <iostream>
#include <math.h>
using namespace std;

struct Point {
    double x, y;
};


void print_point(Point p)
{
    cout << "(" << p.x << ", " << p.y << ")" << endl;
}

double distance(Point p1, Point p2)
{
    double dx = p2.x - p1.x;
    double dy = p2.y - p1.y;

    return sqrt(dx*dx + dy*dy);
}

int main()
{
    Point p1 = {2.0, 2.0};
    Point p2 = {5.0, 6.0};

    print_point(p1);
    print_point(p2);

    cout << "The distance between the points is: ";
    cout  << distance(p1, p2) << endl;

    return 0;
}
