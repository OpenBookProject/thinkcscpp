struct Time {
    // instance variables
    int hour, minute;
    double second;

    // constructors
    Time(double secs);
    Time(int h, int m, double s);
    Time(int h, int m, int s);

    // modifiers 
    void increment(double secs);

    // functions
    void print() const;
    bool after(const Time& t2) const;
    Time add(const Time& t2) const;
    double convert_to_seconds() const;
};
