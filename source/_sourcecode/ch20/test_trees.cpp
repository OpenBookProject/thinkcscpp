#include <iostream>
#include "Tree.h"
using namespace std;

int main()
{
    Tree<int>* tree = new Tree<int>(1, NULL, NULL);
    tree->left = new Tree<int>(2, NULL, NULL);
    tree->right = new Tree<int>(3, NULL, NULL);

    return 0;
}
