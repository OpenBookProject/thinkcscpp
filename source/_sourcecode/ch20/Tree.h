#include <iostream>
using namespace std;

template <class T>
class Tree {
  public:
    T cargo;
    Tree<T>* left;
    Tree<T>* right;

    Tree (T cargo, Tree<T>* left, Tree<T>* right) {
        this->cargo = cargo;
        this->left = left;
        this->right = right;
    }
};
