#include <iostream>
using namespace std;


struct Point {
    double x, y;
};


ostream& operator << (ostream& os, const Point& p) {
    os << "(" << p.x << ", " << p.y << ")";
    
    return os;
}


int main()
{
    Point p = {3, 4};
    Point* pp = &p;

    *pp.x = 5;

    cout << *pp << endl;

    return 0;
}
