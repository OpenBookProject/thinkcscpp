#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
    char letter = 'a';
    char* cp = &letter;

    cout << "letter: " << letter << endl;
    cout << "address of letter: " << long(&letter) << endl;
    cout << "value of pointer to letter: " << long(cp) << endl;
    cout << "address of pointer to letter: " << long(&cp) << endl;
    cout << "dereferencing the pointer cp gives: " << *cp << endl;
     
    return 0;
}
