#include <iostream>
using namespace std;


int main()
{
    int* ip = new int(42);
    int* tip = ip;

    cout << "ip: " << long(ip) << endl;
    cout << "dereferencing the pointer ip gives: " << *ip << endl;

    ip = new int(5);
    cout << "ip is now pointing to: " << long(ip) << endl;
    cout << "dereferencing the pointer ip gives: " << *ip << endl;
    cout << "tip: " << long(ip) << endl;
    cout << "dereferencing the pointer tip gives: " << *tip << endl;
    cout << "So we can safely delete it." << endl;

    delete tip;

    return 0;
}
