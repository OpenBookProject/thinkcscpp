#include <iostream>
using namespace std;


struct Point {
    double x, y;

    Point() {
        x = 0;
        y = 0;
    }

    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }
};


ostream& operator << (ostream& os, const Point& p) {
    os << "(" << p.x << ", " << p.y << ")";
    
    return os;
}


int main()
{
    Point p(3, 4);
    Point* pp = new Point;

    pp->x = 0;
    pp->y = 0;

    cout << p << endl;

    return 0;
}
