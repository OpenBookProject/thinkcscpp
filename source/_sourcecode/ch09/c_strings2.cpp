#include <iostream>
using namespace std;

int main()
{
    // Create a C string
    char s[9] = "C string";
    s[0] = 'C';
    s[1] = ' ';
    s[2] = 's';
    s[3] = 't';
    s[4] = 'r';
    s[5] = 'i';
    s[6] = 'n';
    s[7] = 'g';
    s[8] = '\0';
    // print the C at index 2
    cout << s[2] << endl;
    // print the entire string
    int i = 0;
    while (s[i])
        cout << s[i++];
    cout << endl;
    // print again using pointers 
    for (char* cp = s; *cp != '\0'; cout << *cp++);
    cout << endl;
    return 0;
}
