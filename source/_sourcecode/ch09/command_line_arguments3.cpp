#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
    int i = 0;
    for (char** clp = argv; *clp != nullptr; clp++) {
        cout << "Command line argument [" << i++ << "] is: ";
        cout << static_cast<char*>(*clp) << endl;
    }
    return 0;
}
