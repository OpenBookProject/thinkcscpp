#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
    for (int i = 0; i < argc; i++) {
        cout << "Command line argument [" << i << "] is: ";
        cout << static_cast<char*>(*argv++) << endl;
    }
    return 0;
}
