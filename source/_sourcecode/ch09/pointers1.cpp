#include <iostream>
using namespace std;


int main()
{
    char letter = 'a';
    char* cp = &letter;

    cout << "letter: " << letter << endl;
    cout << "address of letter: " << long(&letter) << endl;
    cout << "pointer to letter: " << long(cp) << endl;
    cout << "dereferencing the pointer cp gives: " << *cp << endl;

    return 0;
}
