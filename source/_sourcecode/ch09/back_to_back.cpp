#include <iostream>
using namespace std;

int main()
{
    int nums1[5] = {1, 2, 3, 4, 5}; 
    int nums2[5] = {6, 7, 8, 9, 0}; 

    cout << "address of nums1: " << nums1 << endl;
    cout << "address of nums2: " << nums2 << endl;

    int offset = nums2 - nums1;
    cout << "distance from nums2 to nums1: " << offset << endl;


    for (int i = 0; i < 5; i++) {
        cout << "nums1[" << i << "] contains: " << nums1[i] << endl;
    }

    for (int i = 0; i < 5; i++) {
        cout << "nums1[" << i << "+offset] contains: " << nums1[i+offset];
        cout << endl;
    }

    return 0;
}
