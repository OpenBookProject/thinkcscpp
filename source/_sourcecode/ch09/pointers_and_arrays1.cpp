#include <iostream>
using namespace std;


int main()
{
    char letters[26];

    for (char letter = 'a'; letter <= 'z'; letter++) {
        letters[letter - 'a'] = letter;
    }
    for (int i = 0; i < 26; i += 2) {
        cout << char(letters[i]);
    }
    cout << endl; 

    return 0;
}
