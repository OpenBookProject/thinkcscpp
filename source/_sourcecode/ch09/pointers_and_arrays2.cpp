#include <iostream>
using namespace std;


int main()
{
    int* nums = (int*)malloc(10 * sizeof(int));

    for (char i = 0; i < 10; i++) {
        *(nums + i) = i * i;
    }

    for (int i = 0; i < 10; i++) {
        cout << *(nums + i);
        if (i < 9) cout << " ";
    }
    cout << endl; 

    return 0;
}
