#include <iostream>
using namespace std;

double absolute_value(double x) {
    if (x < 0) {
       return -x;
    } else if (x > 0) {
        return x;
    }
}

int main() {
    cout << "The absolute value of 5 is " << absolute_value(5) << "." << endl;
    cout << "The absolute value of -5 is " << absolute_value(-5) << "." << endl;
    cout << "The absolute value of 0 is " << absolute_value(0) << "." << endl;
    return 0;
}
