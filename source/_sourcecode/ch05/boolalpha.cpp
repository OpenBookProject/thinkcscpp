#include <iostream>
using namespace std;

bool is_single_digit(int d) {
    return (d >= 0 && d < 10);
}

int main() {
    int num1 = 6;
    int num2 = 42;
    // default
    cout << num1 <<  " single digit? " << is_single_digit(num1) << endl;
    cout << num2 <<  " single digit? " << is_single_digit(num2) << endl;
    // set boolalpha flag
    cout << boolalpha;
    cout << num1 <<  " single digit? " << is_single_digit(num1) << endl;
    cout << num2 <<  " single digit? " << is_single_digit(num2) << endl;
    return 0;
}
