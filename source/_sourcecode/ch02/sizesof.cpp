#include <iostream>
using namespace std;

int main()
{
    cout << "char: " << sizeof(char) << endl;
    cout << "a: " << sizeof 'a' << endl;
    cout << "int: " << sizeof(int) << endl;
    cout << "42: " << sizeof 42 << endl;
    cout << "42000968: " << sizeof 42000968 << endl;
    cout << "Hello!: " << sizeof "Hello!" << endl;

    return 0;
}
