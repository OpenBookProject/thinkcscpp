#include <iostream>
using namespace std;

// output more than one line
int main()
{
    cout << "Hello world." << endl;     // output one line
    cout << "How are you?" << endl;     // output another 
    return 0;
}
