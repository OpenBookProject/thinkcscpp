#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <string>
#include <stack>
using namespace std;

TEST_CASE("Test basic s operations on s of strings") {
    stack<string> s;
    s.push("cheese");
    CHECK(s.top() == "cheese");
    s.push("anchovies");
    CHECK(s.top() == "anchovies");
    s.push("onions");
    CHECK(s.top() == "onions");
    s.pop();
    CHECK(s.top() == "anchovies");
    CHECK(s.empty() == false);
    s.pop();
    CHECK(s.top() == "cheese");
    s.pop();
    CHECK(s.empty() == true);
}
