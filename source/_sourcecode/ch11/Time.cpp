#include <iostream>
#include <string>
#include "Time.h"
using namespace std;

Time::Time(int h, int m, double s) { 
    hour = h;
    minute = m;
    second = s;
}

Time::Time(double s) { 
    hour = int(s / 3600.0);
    s -= hour * 3600.0;
    minute = int(s / 60.0);
    s -= minute * 60;
    second = s;
}

void Time::increment(double s) {
    second += s;

    while (second >= 60.0) {
        second -= 60.0;
        minute += 1;
    }
    while (minute >= 60) {
        minute -= 60;
        hour += 1;
    }
}

void Time::print() const {
    cout << hour << ":" << minute << ":" << second;
}

string Time::to_string() const {
    string s = std::to_string(hour) + ":";
    s += std::to_string(minute) + ":" + std::to_string(int(second));

    return s;
}

bool Time::after(const Time& time2) const {
    if (hour > time2.hour) return true;
    if (hour < time2.hour) return false;

    if (minute > time2.minute) return true;
    if (minute < time2.minute) return false;

    return (second > time2.second);
}

Time Time::add(const Time& t2) const {
    return Time(convert_to_seconds() + t2.convert_to_seconds());
}

double Time::convert_to_seconds() const {
    return (hour * 60 + minute) * 60 + second;
}
