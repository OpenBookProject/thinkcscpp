#include <iostream>
#include "Time.h"
using namespace std;

int main()
{
    Time current_time(9, 14, 30.0);
    current_time.increment(500.0);
    cout << current_time.to_string() << endl;

    Time bread_time(3, 35, 0.0);
    Time done_time = current_time.add(bread_time);
    cout << done_time.to_string() << endl;

    if (done_time.after(current_time)) {
        cout << "The bread will be done after it starts." << endl;
    }

    return 0;
}
