#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <iostream>
#include <string>
#include "Point.h"
using namespace std;

TEST_CASE("Test can create Points") {
    Point t1;
    CHECK(t1.x == 0.0);
    CHECK(t1.y == 0.0);
    Point t2(3, 4);
    CHECK(t2.x == 3.0);
    CHECK(t2.y == 4.0);
}

TEST_CASE("Test can add Points") {
    Point t1(3, 4);
    Point t2(5, 2);
    Point t3 = t1 + t2;
    CHECK(t3.x == 8.0);
    CHECK(t3.y == 6.0);
}

TEST_CASE("Test can render Points as strings") {
    Point t(8, 7);
    string expected = "(8.000000, 7.000000)";
    CHECK(t.to_string() == expected);
}
