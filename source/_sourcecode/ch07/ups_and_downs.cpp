#include <iostream>
#include <string>
#include <cctype>
using namespace std;

string string_to_upper(string s)
{
    string upstr = s;
    int i = 0;

    while (i < s.length()) {
        upstr[i] = toupper(s[i]);
        i++;
    }

    return upstr;
}

string string_to_lower(string s)
{
    string lowstr = s;
    int i = 0;

    while (i < s.length()) {
        lowstr[i] = toupper(s[i]);
        i++;
    }

    return lowstr;
}


int main()
{
    string str;

    cout << "Enter a string: ";
    getline(cin, str);
    cout << "Here is your string in upper case: " << endl;
    cout << string_to_upper(str) << endl;
    cout << "and in lower case: " << endl;
    cout << string_to_lower(str) << endl;
    cout << "with your string unchanged: " << endl;
    cout << str << endl;

    return 0;
}
