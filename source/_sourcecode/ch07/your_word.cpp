#include <iostream>
#include <string>
using namespace std;

int main()
{
    string word;

    cout << "Enter your word: ";
    getline(cin, word);

    if (word < "banana") {
        cout << "Your word, " << word << ", comes before banana." << endl;
    } else if (word > "banana") {
        cout << "Your word, " << word << ", comes after banana." << endl;
    } else {
        cout << "Yes, we have no bananas!" << endl;
    }

    return 0;
}
