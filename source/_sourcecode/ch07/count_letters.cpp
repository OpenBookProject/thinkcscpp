#include <iostream>
#include <string>
using namespace std;

int count_letters(string s, char c)
{
    int count = 0;
    int index = s.find(c);

    while (index != -1) {
        count++;
        index = s.find(c, index+1);
    }

    return count;
}

int main()
{
    string word;
    char letter;

    cout << "Enter your word: ";
    getline(cin, word);

    cout << "Enter a letter: ";
    cin >> letter;

    cout << "'" << letter << "' appears in \"" << word << "\" ";
    cout << count_letters(word, letter) << " times." << endl;

    return 0;
}
