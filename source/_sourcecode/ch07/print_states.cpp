#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    ifstream myfile("resources/states.txt");
    if (!myfile.is_open()) {
        cerr << "Error opening file!" << endl;
        return 1;
    }
    string state;
    while (getline(myfile, state)) {
        cout << state << endl;
    }
    myfile.close();
    return 0;
}
