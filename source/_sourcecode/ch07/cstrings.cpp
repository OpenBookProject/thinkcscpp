#include <iostream>
#include <string.h>
using namespace std;

int main()
{
    char suffix[] = "ack"; 
    char letter = 'J'; 
    cout << "suffix is: " << suffix << endl;
    cout << "letter is: " << letter << endl;
    cout << "letter + suffix is: ";
    cout << letter + suffix << endl;

    return 0;
}
