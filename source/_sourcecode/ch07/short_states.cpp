#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    string state;
    ifstream infile("resources/states.txt");
    if (!infile.is_open()) {
        cerr << "Error opening input file!" << endl;
        return 1;
    }
    ofstream outfile("resources/short_states.txt");
    if (!outfile.is_open()) {
        cerr << "Error opening output file!" << endl;
        return 1;
    }
    while (getline(infile, state)) {
        if (state.length() < 6) {
            outfile << state << endl;
        }
    }
    infile.close();
    outfile.close();
    return 0;
}
