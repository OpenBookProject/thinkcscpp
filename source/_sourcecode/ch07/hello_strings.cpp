#include <iostream>
#include <string>
using namespace std;

int main()
{
    // string variables
    string str1;
    str1  = "Hello, ";
    string str2 = "strings!";
    cout << str1 << str2 << endl;

    // extracting characters from a string
    string fruit = "banana";
    char first = fruit[0];
    cout << first << endl;

    // getting last character from a string
    int size = fruit.length();
    char last = fruit[size-1];
    cout << last << endl;

    // find
    int index = fruit.find('a');
    cout << "Find a in banana: " << index << endl;
    index = fruit.find('x');
    cout << "Find x in banana: " << index << endl;
    index = fruit.find("nana");
    cout << "Find nana in banana: " << index << endl;
    index = fruit.find('a', 2);
    cout << "Find a in banana starting at 2: " << index << endl;

    // concatenation
    string fruit2 = "apple";
    string baked_good = " nut bread";
    string dessert = fruit2 + baked_good;
    cout << dessert << endl;

    // can't concatenate C strings, so the following won't work:
    // string testStr = "This" + " and that";

    // strings are mutable

    string greeting = "Hello, world!";
    greeting[0] = 'J';
    cout << greeting << endl;

    return 0;
}
