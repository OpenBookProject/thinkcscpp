#include <iostream>
#include <string>
#include <ios>
#include <limits>
using namespace std;

int main()
{
    // Read a number
    int x;
    cout << "Enter a number: ";
    cin >> x;
    cout << "Your input state, cin.good(), is: " << cin.good() << endl;
    cout << "Your number is: " << x << endl;

    // Read a string 
    string s;
    cout << "Enter your name: ";
    cin >> s;
    cout << "Your name is: " << s << endl;

    // Clear remaining characters in input buffer to newline
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    // Read a line
    string s2;
    cout << "Enter your full name: ";
    getline(cin, s2);
    cout << "Your full name is: " << s2 << endl;

    return 0;
}
