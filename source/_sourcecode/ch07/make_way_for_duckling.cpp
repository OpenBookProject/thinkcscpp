#include <iostream>
#include <string>
using namespace std;

int main()
{
    string suffix = "ack"; 
    char letter = 'J'; 
    while (letter <= 'Q') {
        cout << letter + suffix << endl;
        letter++;
    }

    return 0;
}
