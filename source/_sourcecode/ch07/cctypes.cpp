#include <iostream>
#include <string>
#include <cctype>
using namespace std;

int main()
{
    char ch = 'a';
    if (isalpha(ch)) {
        cout << "The character " << ch << " is a letter." << endl; 
        ch = toupper(ch);
        cout << "In upper case it is: " << ch << "." << endl; 
    }

    return 0;
}
