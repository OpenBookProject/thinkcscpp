#include <iostream>
#include <string>
using namespace std;

int count_letters(string s, char c, int start = -1)
{
    if ((start = s.find(c, start+1)) == -1) return 0;
    return 1 + count_letters(s, c, start);
}

int main()
{
    string word;
    char letter;

    cout << "Enter your word: ";
    getline(cin, word);

    cout << "Enter a letter: ";
    cin >> letter;

    cout << "'" << letter << "' appears in \"" << word << "\" ";
    cout << count_letters(word, letter) << " times." << endl;

    return 0;
}
