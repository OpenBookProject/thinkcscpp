void CardCollection::swap_cards(int i, int j)
{
    Card temp = cards[i];
    cards[i] = cards[j];
    cards[j] = temp;
}
