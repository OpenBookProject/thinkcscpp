#include "Player.h"
#include "Hand.h"
#include "Eights.h"
#include <iostream>
using namespace std;

Card Player::play(Eights& eights, const Card& prev)
{
    Card card;
    try {
        card = search_for_match(prev);
    } catch(const char* msg) {
        // no match found; player draws a card
        card = draw_for_match(eights, prev);
    }
    return card;
}

Card Player::search_for_match(const Card& prev)
{
    for (int i = 0; i < hand.size(); i++) {
        Card card = hand.get_card(i);
        if (card_matches(card, prev)) {
            return hand.pop_card(i);
        }
    }
    throw "No match found";
}

Card Player::draw_for_match(Eights& eights, const Card& prev)
{
    while (true) {
        Card card = eights.draw_card();
        cout << name << " draws " << card.to_string() << endl;
        if (card_matches(card, prev)) {
            return card;
        }
        hand.add_card(card);
    }
}

bool Player::card_matches(const Card& card1, const Card& card2)
{
    return card1.suit == card2.suit
        || card1.rank == card2.rank
        || card1.rank == 8;
}

void Player::display()
{
    hand.display();
}
