#include "Hand.h"
#include <string>
#include <iostream>
using namespace std;

Hand::Hand(string label) : CardCollection(label) {
    
}
void Hand::display() {
    cout << label << ": \n";
    for (int i = 0; i < size(); i++) {
        cout << get_card(i).to_string() << "\n";
    }
    cout << endl;
}