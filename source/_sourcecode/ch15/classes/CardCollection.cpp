#include <string>
#include "CardCollection.h"
#include "randomutils.h"

CardCollection::CardCollection(string label) {
    this->label = label;
    // this->cards is automatically initialized to an empty vector
}

void CardCollection::add_card(const Card& card)
{
    cards.push_back(card);
}

Card CardCollection::pop_card()
{
    // remove last card
    Card toRemove = cards.back();
    cards.pop_back();
    return toRemove;
}

Card CardCollection::pop_card(int i)
{
    Card toRemove = cards[i];
    cards.erase(cards.begin() + i);
    return toRemove;
}

bool CardCollection::is_empty()
{
    return cards.empty();
}

int CardCollection::size()
{
    return cards.size();
}

Card CardCollection::get_card(int i)
{
    return cards[i];
}

Card CardCollection::last_card()
{
    return cards.back();
}

void CardCollection::swap_cards(int i, int j)
{
    Card temp = cards[i];
    cards[i] = cards[j];
    cards[j] = temp;
}

void CardCollection::shuffle()
{
    for (int i = cards.size() - 1; i > 0; i--) {
        int j = random_between(0, i + 1);
        swap_cards(i, j);
    }
}

void CardCollection::deal(CardCollection& that, int n)
{
    for (int i = 0; i < n; i++) {
        Card card = pop_card();
        that.add_card(card);
    }
}

void CardCollection::deal_all(CardCollection& that)
{
    int n = cards.size();
    deal(that, n);
}
