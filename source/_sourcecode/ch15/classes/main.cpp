#include "Deck.h"
#include "Hand.h"
#include "Eights.h"
#include <iostream>
#include "randomutils.h"
using namespace std;

int main() {
    init_random_seed();
    
    // test from end of 16.3:
    // Deck deck = Deck("Deck");
    // deck.shuffle();

    // Hand hand = Hand("Hand");
    // deck.deal(hand, 5);

    // hand.display();
    // Hand drawPile = Hand("Draw Pile");
    // deck.deal_all(drawPile);

    // cout << "Draw Pile has "<< drawPile.size() << " cards." << endl;

    // test the full game:
    cout << "Starting a new Crazy Eights game:\n" << endl;
    Eights game = Eights();
    game.play_game();
}