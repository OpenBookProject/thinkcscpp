#pragma once
#include "Card.h"
#include "Hand.h"
#include <string>
using namespace std;

class Eights;

struct Player {
    string name;
    Hand hand;

    Player(string name) : name(name), hand(Hand(name)) {};
    
    Card play(Eights& eights, const Card& prev);
    Card search_for_match(const Card& prev);
    Card draw_for_match(Eights& eights, const Card& prev);
    bool card_matches(const Card& card1, const Card& card2);
    void display();
};