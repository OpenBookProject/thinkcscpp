#include <iostream>
#include <vector>
#include <string>
#include "Card.h"
using namespace std;

Card::Card()
{
    suit = 0; rank = 0;
}

Card::Card(int s, int r)
{
    suit = s; rank = r;
}

string Card::to_string() const
{
    string rank_strings[] = {"", "Ace", "2", "3", "4", "5", "6", "7",
                                   "8", "9", "10", "Jack", "Queen", "King"};
    string suit_strings[] = {"Clubs", "Diamonds", "Hearts", "Spades"};
    
    return rank_strings[rank] + " of " + suit_strings[suit];
}

bool Card::equals(const Card& c2) const
{
    return (suit == c2.suit && rank == c2.rank);
}

bool Card::is_greater(const Card& c2) const
{
    // first check the suit_strings
    if (suit > c2.suit) return true;
    if (suit < c2.suit) return false;

    // if suit_strings are equal, check rank_strings 
    if (rank > c2.rank) return true;
    if (rank < c2.rank) return false;

    // if rank_strings are equal too, 1st card is not greater than the 2nd 
    return false;
}
