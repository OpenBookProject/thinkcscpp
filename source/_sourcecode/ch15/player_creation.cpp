struct Player {
    string name;
    Hand hand;

    Player(string name) {
        this->name = name;
        this->hand = Hand(name);
    }
};
