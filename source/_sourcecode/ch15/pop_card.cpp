Card CardCollection::pop_card()
{
    // remove last card
    Card toRemove = cards.back();
    cards.pop_back();
    return toRemove;
}
