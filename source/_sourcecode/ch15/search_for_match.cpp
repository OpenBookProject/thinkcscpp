Card Player::search_for_match(const Card& prev)
{
    for (int i = 0; i < hand.size(); i++) {
        Card card = hand.get_card(i);
        if (card_matches(card, prev)) {
            return hand.pop_card(i);
        }
    }
    throw "No match found";
}
