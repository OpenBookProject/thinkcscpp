Card Eights::draw_card()
{
    if (draw_pile.is_empty()) {
        reshuffle();
    }
    return draw_pile.pop_card();
}
