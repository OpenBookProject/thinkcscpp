bool Eights::is_done()
{
    return one.hand.is_empty() || two.hand.is_empty();
}
