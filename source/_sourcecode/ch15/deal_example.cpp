Deck deck = Deck("Deck");
deck.shuffle();

Hand hand = Hand("Hand");
deck.deal(hand, 5);
hand.display();

Hand draw_pile = Hand("Draw Pile");
deck.deal_all(draw_pile);
cout << "Draw Pile has " << draw_pile.size() << "cards." << endl;
