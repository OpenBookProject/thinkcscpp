void Eights::reshuffle()
{
    Card prev = discard_pile.pop_card();
    discard_pile.deal_all(draw_pile);
    discard_pile.add_card(prev);
    draw_pile.shuffle();
}
