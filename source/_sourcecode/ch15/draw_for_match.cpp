Card Player::draw_for_match(Eights& eights, const Card& prev)
{
    while (true) {
        Card card = eights.draw_card();
        cout << name << " draws " << card.to_string() << endl;
        if (card_matches(card, prev)) {
            return card;
        }
        hand.add_card(card);
    }
}
