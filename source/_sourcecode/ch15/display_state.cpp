void Eights::display_state()
{
    one.display();
    two.display();
    discard_pile.display();
    cout << "Draw pile:" << endl;
    cout << draw_pile.size() << " cards" << endl;
    
    // wait for user input before continuing
    getchar();

    cout << "--------\n" << endl;
}
