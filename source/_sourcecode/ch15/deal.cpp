void CardCollection::deal(CardCollection& that, int n)
{
    for (int i = 0; i < n; i++) {
        Card card = pop_card();
        that.add_card(card);
    }
}
