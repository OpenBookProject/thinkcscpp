void Eights::play_game()
{
    Player* player = &one;
    // keep playing until there's a winner
    while (!is_done()) {
        display_state();
        take_turn(player);
        player = next_player(player);
    }
    // display the final score
    one.display();
    two.display();
}
