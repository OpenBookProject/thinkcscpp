Card Player::play(Eights& eights, const Card& prev)
{
    Card card;
    try {
        card = search_for_match(prev);
    } catch(const char* msg) {
        // no match found; player draws a card
        card = draw_for_match(eights, prev);
    }
    return card;
}
