void Eights::take_turn(Player* player)
{
    Card prev = discard_pile.last_card();
    Card next = (*player).play(*this, prev);
    discard_pile.add_card(next);
    cout << (*player).name << " plays " << next.to_string() << endl << endl;
}
