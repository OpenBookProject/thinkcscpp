struct CardCollection {
    string label;
    vector<Card> cards;

    CardCollection(string label) {
        this->label = label;
        // this->cards is automatically initialized to an empty vector
    }
};
