struct Hand: CardCollection {
    Hand(string label): CardCollection(label) { }

    void display() {
        cout << get_label() << ": " << endl;
        for (int i = 0; i < size(); i++) {
            cout << get_card(i).to_string() << endl;
        }
        cout << endl;
    }
};
