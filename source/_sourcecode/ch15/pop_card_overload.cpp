Card CardCollection::pop_card(int i)
{
    Card toRemove = cards[i];
    cards.erase(cards.begin() + i);
    return toRemove;
}
