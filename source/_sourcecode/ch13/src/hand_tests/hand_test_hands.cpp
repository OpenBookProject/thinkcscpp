#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include "../Cards.h"
#include "../random.h"
using namespace std;

int main()
{
    srand(time(0));
    Deck deck;
    cout << "\nShuffled Deck:" << endl;
    deck.shuffle();
    deck.print();

    Deck hand1 = deck.subdeck(0, 4);
    Deck hand2 = deck.subdeck(5, 9);
    Deck pack = deck.subdeck(10, 51);

    cout << "\nHand 1:" << endl;
    hand1.print();
    cout << "\nHand 2:" << endl;
    hand2.print();

    return 0;
}
