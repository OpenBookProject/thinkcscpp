#include <iostream>
#include "../random.h"
using namespace std;

int main()
{
    // Seed with current time
    srand(time(0));

    for (int i = 0; i < 20; i++) {
        int low = random_between(0, 10);
        int high = random_between(11, 100);
        cout << "Random number between " <<  low << " and " << high << ": ";
        cout << random_between(low, high) << endl;
    }

    cout << "Your rolled a: ";
    for (int i = 0; i < 20; i++) {
        cout << random_between(1, 6) << ' ';
    }
    cout << endl;

    return 0;
}
