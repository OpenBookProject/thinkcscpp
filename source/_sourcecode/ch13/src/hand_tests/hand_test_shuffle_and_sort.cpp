#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include "../Cards.h"
#include "../random.h"
using namespace std;

int main()
{
    srand(time(0));
    Deck deck;
    cout << "The Deck:" << endl;
    deck.print();

    Card card1;
    Card card2(SPADES, ACE);
    cout << "Card 1: " << card1.to_string() << endl;
    cout << "Card 2: " << card2.to_string() << endl;
    cout << card2.find(deck) << endl;

    cout << "\nShuffled Deck:" << endl;
    deck.shuffle();
    deck.print();
    cout << "\nSorted Deck:" << endl;
    deck.sort();
    deck.print();

    return 0;
}
