# How to run

Since the files being tested are in the parent directory, the local includes
have been prefixed with ``../``, and the ``.cpp`` files must be listed
similarly:
```
$ g++ hand_test_hand.cpp ../Cards.cpp ../random.cpp
```
