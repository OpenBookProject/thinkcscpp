#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int b = 128;
    int exp = 7;
    while (b > 0) {
        cout << "2^" << exp << "\t"; 
        b = b / 2;
        exp = exp - 1;
    }
    cout << endl;
    return 0;
}
