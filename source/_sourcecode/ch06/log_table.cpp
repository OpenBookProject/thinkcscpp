#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int x = 1;
    do {
        cout << x << '\t' << log(x) << endl;
    } while (++x < 10);
    return 0;
}
