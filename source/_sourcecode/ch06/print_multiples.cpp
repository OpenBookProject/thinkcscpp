#include <iostream>
#include <math.h>
using namespace std;

void print_multiples(int n, int upper)
{
    int i = 1;
    while (i <= upper) {
        cout << n * i << "   ";
        i = i + 1;
    }
    cout << endl;
}

void print_mult_table(int high)
{
    int i = 1;
    while (i <= high) {
        print_multiples(i, i);
        i = i + 1;
    }
    cout << endl;

}

int main()
{
    print_mult_table(7);
    return 0;
}
