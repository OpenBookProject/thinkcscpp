#include <time.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

int main() {
    srand(time(NULL));
    unsigned int m1, m2, prod, num_correct = 0;
    bool correct;

    cout << "\nOkay, let's see how many multiplication questions in ";
    cout << "a row you can get right." << endl;
    cout << "I'll keep asking you questions until you get one wrong." << endl;
    cout << "I'll also keep count for you. Ready? Let's go...\n" << endl;
    do {
        m1 = rand() % 10; 
        m2 = rand() % 10; 
        cout << "What is " << m1 << " times " << m2 << "? ";
        cin >> prod;
        correct = prod == m1 * m2;
        if (correct) {
            cout << "That's right. Well done!" << endl;
            num_correct++;
        }
        else
            cout << "No, the correct answer is " << m1 * m2 << "." << endl;
    } while (correct);
    cout << "You got " << num_correct << " questions right in a row.";
    cout << " Nice job!" << endl;
    return 0;
}
