Preface
=======

It is now almost a quarter century since `Allen Downey
<https://en.wikipedia.org/wiki/Allen_B._Downey>`_ first released his textbook,
*How to think like a computer scientist*, under the `GNU General Public License
<https://en.wikipedia.org/wiki/GNU_General_Public_License>`_, providing me with
a resource I could use to teach Python to my high school computer science
students at a time when no textbook existed for teaching introductory computer
science with Python.

Twenty-five years later I am once again working on a re-mix of the book, this
time to develop it into a C++ textbook for use in two `dual-enrolled
<https://en.wikipedia.org/wiki/Dual_enrollment>`_ courses at the Arlington
Career Center that are part of the Virginia Community College System's new
computer science sequence, `Object-Oriented Programming
<https://courses.vccs.edu/courses/CSC222-ObjectOrientedProgramming>`_ and `Data
Structures and Analysis of Algorithms
<https://courses.vccs.edu/courses/CSC223>`_.

This effort began with a trip down memory lane, when I found a pdf on the
`Universidade Federal do Paraná <https://web.inf.ufpr.br/dinf/>`_ website of a
version of the book that two of my high school seniors, Paul Bui and Jonah
Cohen, worked on back in 2001 as a resource for our `AP Computer Science AB
<https://en.wikipedia.org/wiki/AP_Computer_Science_A#AP_Computer_Science_AB>`_
course at Yorktown High School in Arlington, Virginia. Paul has now been a
computer science colleague with me in Arlington Public Schools for more than 15
years, and kindly offered to contribute the :ref:`forward` to this new edition
of our book.

I can't begin to communicate how much fun it has been for me personally to
work on this. As I learned so many years ago now, the best way to learn a
new computer programming language is to use it to write a new remix of Allen
Downey's wonderful book!

.. note::

   In the Fall semester of 2024 I added a new goal for this book, to help
   prepare students for the `CPE – C++ Certified Entry-Level Programmer
   Certification <https://cppinstitute.org/cpe>`_. With the many changes that
   will involve, including shifts in both emphasis and focus, I decided that in
   order to protect Allen Downey's stellar reputation I should rename the book
   and list myself as the author, so that blame for all the errors and bad
   decisions I make in it can be directed at me. As of this writing, most of
   the text is still his, but that will gradually change in the years ahead. 

My main motivation for this effort is the desire to switch from Java to C++
as the "other programming language" in our Associate Degree in Computer Science
program at Arlington Tech. There are two main reasons I wanted to do this now:

* I want *demystifying the computer* to be one of the overarching goals of our
  program. I want students to have to do some memory management, to use
  pointers explicitly, and to be allowed to "shoot themselves in the foot"
  along their journey learning to program. C++ (and C, which we will be
  learning in our `Computer Systems
  <https://courses.vccs.edu/colleges/nova/courses/CSC215-ComputerSystems/>`_
  class) will enable them to do all that.

* With the growing interest in microprocessors for uses like
  `TinyML <https://www.tinyml.org/>`_, C and C++ make more sense again.

It is my goal to create `OER
<https://www.unesco.org/en/open-educational-resources>`_ for all of the courses
in our computer science sequence.  This book will fullfil that goal for two of
our courses.  It is such a pleasure to be able to teach a course with *just
the book you always wanted*. By having my own book to change and adapt along
with my students, I can get just that.

| Jeffrey Elkner
| Arlington Career Center 
| Arlington, Virginia
| 7 September 2024
