..  Copyright (C)  Jeffrey Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with Invariant Sections being Foreward, Preface, and Contributor List, no
    Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is
    included in the section entitled "GNU Free Documentation License".

.. _code_source:

Appendix A: Code Source
========================

Source code for the ``LinkedList``, ``Stack``, and other examples from the
latter part of the book is included here for your convenience.


``LinkedList.h``
----------------

.. literalinclude :: _sourcecode/app_a/LinkedList.h
   :language: cpp


``Stack.h``
-----------

.. literalinclude :: _sourcecode/app_a/Stack.h
   :language: cpp
