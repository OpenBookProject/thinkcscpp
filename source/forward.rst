.. _forward:

Forward
=======

By Paul Bui 

Feel free to skip ahead to the core content of the book if you wish to avoid my
exercise in nostalgia. My high school computer science teacher, Jeff Elkner,
was/is a firm proponent for open source software. I’m pretty sure our computer
lab in 1998 used the RedHat Linux distribution and my first “development
environment” consisted of the vi text editor and g++, GNU’s C++ compiler. Jeff
had/has a lot of confidence (and trust) in his students as well as the open
source movement, so our lab that ran on open source software was also managed
by his own students. 

It came to no surprise that when Allen Downey released his open textbook: How
to Think Like a Computer Scientist, Jeff was one of the first in line to adopt
the text for our classes. It’s been about 25 years since my high school
computer science class was given How to Think Like a Computer Science. So what
language(s) did we learn? Well...

From what I remember, the most popular programming languages taught in high
schools at the time were Pascal and C++. The College Board’s AP Computer
Science curriculum used the Pascal programming language from 1984 to 1998, and
then C++ from 1998 to 2003 (thank you Wikipedia for remembering better than I
do). Let’s not forget that Java jumped onto the scene in the mid-1990s and was
gaining popularity. In 2004, AP CS switched to Java, and continues to use Java
to this day (it’s 2023 right now). So by the late 1990s and early 2000s, there
were primarily two programming languages taught in high schools and colleges:
C++ and Java. Yes, Python was around too, but let’s pretend that it was still
just a background character and the spotlight did not shine on it yet.

I had to look back at an old post that I wrote back in 2000 to remember this
part, but Mr. Downey had written several chapters of the Java version of his
book about stacks, queues, and heaps…which were not yet added to the C++
version. Jeff, or as I called him, “Mr. Elkner” since I was still only in high
school, recruited myself and another student, Jonah Cohen, to write a few new
chapters in the C++ version as well as porting over the stacks, queues, and
heap chapters. Jonah wrote a chapter on object-oriented programming, and I
wrote some chapters on pointers, references, and templated classes. Several
more students in future years continued to maintain and update the book.

Fast forward to 2006. I decided to become a computer science teacher, and I
wanted to teach Python as the introductory language in my classes. Where do I
decide to teach? ... back in Arlington County, VA ... the same school district
that I grew up in, and Jeff was still teaching at the same high school from
which I graduated. Lucky for me, Jeff had finished porting Mr. Downey’s book to
Python.  For the next five or so years, I used the Python version of How to
Think Like a Computer Scientist as my primary textbook. Hundreds of my own
students benefited from the open textbook, as much as I did when I used it
myself. 

The C++ version of the book was deprecated when Java and Python became the
primary introductory programming languages…or so I thought. It’s now 2023 and
Jeff is reviving revising the C++ version of Mr. Downey’s book so that he can
use it for this newer generation of budding software developers. 

As for myself, I haven’t written C++ code in over a decade, but rereading parts
of this book felt like I was catching up with an old friend from high school. I
hope you enjoy it too.

| Paul Bui 
| Washington-Liberty High School 
| Arlington, Virginia
| 30 March 2023
