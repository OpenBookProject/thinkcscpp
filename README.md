# A C++ Book for Undergraduate Computer Science Majors 

This book is being written for use in
[CSC 222: Object-Oriented Programming](https://courses.vccs.edu/courses/CSC222-ObjectOrientedProgramming) and
[CSC 223: Data Structures and Analysis of Algorithms](https://courses.vccs.edu/courses/CSC223-DataStructuresandAnalysisofAlgorithms)
courses at
[Arlington Tech](https://arlingtontech.apsva.us/about-arlington-tech/), but is
being shared as [OER](https://en.wikipedia.org/wiki/Open_educational_resources)
in the hope that it may be useful to others.


## Resources

The following resources were really helpful in writing this book.

* [Function Pointers in C and C++](https://www.cprogramming.com/tutorial/function-pointers.html)
